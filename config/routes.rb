Rails.application.routes.draw do
  get '/', to: 'main#index'
  resources 'teams'
  get 'fixtures', to: 'fixtures#index'
  get 'groups', to: 'groups#index'
  post 'login', to: 'logins#create'
  post 'logout', to: 'logins#destroy'
end
