class TeamsController < ApplicationController
  def index
    render json: Team.all
  end
  def show
    @team = Team.find params[:id]
    @fixtures = @team.fixtures
    render json: {
      team: @team, 
      fixtures: @fixtures
    }
  end
end
