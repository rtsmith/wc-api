class MainController < ActionController::API
  def index
    render json: {
      teams: Team.all,
      fixtures: Fixture.all
    }
  end
end
