class LoginsController < ApplicationController
  def create
    if user = User.authenticate(user_params[:token])
      session[:current_user_id] = user['sub']
      render json: user
      # TODO handle errors, like RSI domain error
    end
  end

  def destroy
    @_current_user = session[:current_user_id] = nil
  end

  private
  def user_params
    params.require(:user).permit(:token)
  end
end
