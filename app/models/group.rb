class Group < ApplicationRecord
  has_many :teams
  validates :name, uniqueness: true
  def fixtures
    self.teams.map{|t| t.fixtures}
  end
end
