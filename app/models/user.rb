class User < ApplicationRecord
  validates :google_id, presence: true, uniqueness: true
  before_save :pickTeams #TODO ensure only happens once

  def self.authenticate(token)
    validator = GoogleIDToken::Validator.new
    client_id = '601586664110-52635r2aa6cg7iji7phbdv2roai2l5pu.apps.googleusercontent.com'
    begin
      payload = validator.check(token, client_id)
      if payload['hd'] != "ruralsourcing.com"
        raise StandardError, "Not a Rural Sourcing account"
      end
      User.find_or_create_by(google_id: payload['sub'])
      payload
    rescue GoogleIDToken::ValidationError => e
      "Cannot validate: #{e}"
    end
  end

  def getTeams
    Team.find(self.teams)
  end

  private

  def random(max)
    Random.new.rand(max)
  end
  def pickTeams
    self.teams = Group.all.map{|g| g.teams[random(g.teams.length)].id}
  end
end
