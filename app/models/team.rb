class Team < ApplicationRecord
  belongs_to :group
  validates :name, uniqueness: true
  def fixtures
    Fixture.where("home_team_id = ? OR away_team_id = ?", self.id, self.id)
  end
end
