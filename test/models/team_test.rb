require 'test_helper'

class TeamTest < ActiveSupport::TestCase
  test "team count" do
    assert_equal 3, Team.count
  end
end
