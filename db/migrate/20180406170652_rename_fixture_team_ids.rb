class RenameFixtureTeamIds < ActiveRecord::Migration[5.1]
  def change
    change_table :fixtures do |t|
      t.rename :home_team, :home_team_id
      t.rename :away_team, :away_team_id
    end
  end
end
