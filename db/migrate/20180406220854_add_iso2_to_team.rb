class AddIso2ToTeam < ActiveRecord::Migration[5.1]
  def change
    add_column :teams, :iso2, :string
  end
end
