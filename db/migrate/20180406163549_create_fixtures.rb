class CreateFixtures < ActiveRecord::Migration[5.1]
  def change
    create_table :fixtures do |t|
      t.datetime :start
      t.integer :home_team
      t.integer :away_team
      t.integer :home_goals
      t.integer :away_goals
      t.integer :home_penalties
      t.integer :away_penalties
      t.string :stage

      t.timestamps
    end
  end
end
