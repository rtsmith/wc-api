data = JSON.parse(File.read('db/data.json'))
fixtures = JSON.parse(File.read('db/fixtures.json'))

# make groups
['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'].each do |g|
  Group.create name: g
end

# make teams
data["teams"].each do |t|
  Team.create name: t['name'], iso2: t['iso2'], group: Group.find_by_name(t["group"])
end

fixtures['fixtures'].each do |f|
  hometeam = Team.find_by_name f['homeTeamName']
  awayteam = Team.find_by_name f['awayTeamName']
  Fixture.create home_team: hometeam, away_team: awayteam, start: f['date']
end
